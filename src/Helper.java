public abstract class Helper {

    public static <L,R>String calculations(L left , String operator, R right){
        if(left instanceof Long && !(right instanceof Double)){
            String strA = String.valueOf(left).trim();
            String strB = String.valueOf(right).trim();
            Long numA = Long.parseLong(strA);
            Long numB = Long.parseLong(strB);
            switch (operator){
                case "+" : return String.valueOf(numA + numB);
                case "-" : return String.valueOf(numA - numB);
                case "/" :
                    if(numB == 0){
                        System.out.println("На ноль делить нельзя!");
                        return"";
                    }
                    if(numA < numB){
                        return String.valueOf(Double.valueOf(numA) / Double.valueOf(numB));
                    }else {
                        return String.valueOf(numA / numB);
                    }
                case "*" : return String.valueOf(numA * numB);
            }
        }
        if(left instanceof Double || right instanceof Double){
            String strA = String.valueOf(left);
            String strB = String.valueOf(right);
            Double numA = Double.parseDouble(strA);
            Double numB = Double.parseDouble(strB);
            switch (operator){
                case "+" : return String.valueOf(numA + numB);
                case "-" : return String.valueOf(numA - numB);
                case "/" :
                    if(numB == 0){
                        System.out.println("На ноль делить нельзя!");
                        return"";
                    }
                    return String.valueOf(numA / numB);
                case "*" : return String.valueOf(numA * numB);
            }
        }
        return "";
    }

}
