 final class Const {
     static final String TAG_CALCK = "calc";
     static final String TAG_CONVERTOR = "convertor";
     static final String MESSAGE_NOT_NUMBER = "Неверное число, повторите выбор!";
     static final String MESSAGE_NOT_FORMAT = "Неверный формат ввода, повторите выбор!";
     static final String SOMETHING_WENT_WRONG = "Что-то пошло нет так :) , повторите выбор!";
     static final String RESULT = "Результат => ";
}
