public class Factory {
    private static Factory instance = null;

    private Factory() {
    }

    public static synchronized Factory getInstance() {
        if (instance == null) {
            instance = new Factory();
        }
        return instance;
    }

    public <T,C> IFactory factoryMethod(T callback, C controller, String cmd) {
        switch (cmd) {
            case Const.TAG_CALCK:
                return new Calc<>((Calc.CallBack) callback,(IController) controller);
            default:
                 throw new NullPointerException();
        }
    }
}
