import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Controller implements IController,
        Convertor.CallBack, Calc.CallBack {
    private BufferedReader reader;

    public Controller() {
        reader = new BufferedReader(new InputStreamReader(System.in));
    }

    @Override
    public void run() {
        String cmd = null;
        try {
            System.out.println("Сделайте выбор : "
                    + "\n"
                    + "Калькулятор, выбор число 1 "
                    + "\n"
                    + "Конвертор валют, выбор число 2");
            int selectApp = Integer.parseInt(reader.readLine());
            switch (selectApp) {
                case 1:
                    cmd = Const.TAG_CALCK;
                    break;
                case 2:
                    cmd = Const.TAG_CONVERTOR;
                    break;
                default:
                    cmd = Const.MESSAGE_NOT_NUMBER;
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NumberFormatException e) {
            System.out.println(Const.MESSAGE_NOT_FORMAT);
            run();
        }
        if (cmd == null) {
            System.out.println(Const.SOMETHING_WENT_WRONG);
            run();
        }
        if (cmd != null && cmd.equals(Const.MESSAGE_NOT_NUMBER)) {
            System.out.println(Const.MESSAGE_NOT_NUMBER);
            run();
        }
        try {
            IFactory factory = Factory.getInstance().factoryMethod(this, this, cmd);
            factory.work();
        } catch (Exception e) {
            if (e.getMessage()== null) {
                System.out.println("Конвертор в данный момент не реализованый");
            }
            run();
        }
    }

    @Override
    public void callBack(String value) {
        System.out.println(
                Const.RESULT
                        .concat(" ")
                        .concat(value));
    }

}
