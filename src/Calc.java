

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Калькулятор
 * операци + , - , * , /
 *
 * @param <T>
 */
public class Calc<T extends Calc.CallBack, C extends IController> implements IFactory {
    private T callback;
    private C controller;
    private BufferedReader reader;
    private boolean flagWork = true;

    interface CallBack {
        void callBack(String value);
    }

    public Calc(T callback, C controller) {
        this.callback = callback;
        this.controller = controller;
        reader = new BufferedReader(new InputStreamReader(System.in));

    }

    public void work() {
        String operation = null;
        do {
            System.out.println("Выбирите оператор : "
                    + "\n"
                    + "| + | - | * | / | или введите help ");
            try {
                operation = reader.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (NumberFormatException e) {
                System.out.println(Const.MESSAGE_NOT_FORMAT);
            }
            if (operation != null) {
                if (operation.equals("-") ||
                        operation.equals("+") ||
                        operation.equals("*") ||
                        operation.equals("/") ||
                        operation.equals("help")) {
                    help(operation);
                    workManager(operation);
                } else {
                    System.out.println("вы выбрали неверный оператор");
                }
            }
        } while (flagWork);
    }


    private void workManager(String operation) {
        System.out.println("Введите число :");
        Object numA = calculations();
        System.out.println("Введите число :");
        Object numB = calculations();
        callback.callBack(Helper.calculations(numA, operation, numB));
    }

    private void help(String operation) {
        if (operation == null) {
            System.out.println(Const.SOMETHING_WENT_WRONG);
            return;
        }
        if (operation.equals("help")) {
            System.out.println("Сделайте выбор : "
                    + "\n"
                    + "Вернутся в раздел выбора приложений , выбор число 1 "
                    + "\n"
                    + "Выход из приложения , выбор число 2"
                    + "\n"
                    + "Продолжить работу , выбор число 0");
            int select = 0;
            try {
                select = Integer.parseInt(calculations().toString());
            } catch (NumberFormatException e) {
                return;
            }
            switch (select) {
                case 0: work();
                break;
                case 1:
                    flagWork = false;
                    controller.run();
                    return;
                case 2:
                    System.exit(0);
                    break;
                default:
                    System.out.println("комманда не распознана");
            }
        }
    }

    private Object calculations() {
        Object num = null;
        try {
            num = reader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (num != null) {
            try {
                return Long.parseLong(num.toString());
            } catch (NumberFormatException e) {
                try {
                    return Double.parseDouble(num.toString());
                } catch (NumberFormatException e1) {
                    System.out.println("Ошибка формата ввода, введите число");
                    work();
                }
            }
        }
        return Const.SOMETHING_WENT_WRONG;
    }
}
